<?php

/**
 * DMWS Client Service Class
 */

namespace Drupal\feeds_dm\Service;

use \GuzzleHttp\ClientInterface;
use \GuzzleHttp\Exception\RequestException;
use Drupal\Component\Utility\Html;
use Drupal\Component\Serialization\Json;

class DMWSClient {
	
	/**
	 * An http client.
	 *
	 * @var \GuzzleHttp\ClientInterface
	 */
	protected \GuzzleHttp\ClientInterface $client;
	
	/**
     * Service account username.
     *
     * @var String
     */
	protected string $username = "";
	
	/**
     * Service account password.
     *
     * @var String;
     */
	protected string $password = "";
	
	/**
     * Boolean to used to determine resource URL.
	 * If set to TRUE, the BETA Resource URL is used. If set to TRUE, the production resource URL is used.
	 *
     * @var bool;
     */
	protected bool $beta = false;
	
	/**
     * Constructor
     *
     * @param ClientInterface $client 
	 *    HTTP client.
	 *
	 * @return void
     */
	public function __construct(ClientInterface $client) {
		$this->client = $client;
	}
	
	/** 
	 * Returns the Resource URL for the REST API.
	 *
	 * @return String
	 *    Resource URL.
	 */
	protected function getBaseURL() {
		if ($this->beta) {
			return "https://betawebservices.digitalmeasures.com/login/service/v4/";
		} else {
			return "https://webservices.digitalmeasures.com/login/service/v4/";
		}
	}
	
	/**
	 * Sets the resource URL.
	 *
	 * @param bool $beta 
	 *    If TRUE, BETA Resource URL will be used. Default FALSE (Production.
	 *
	 * @return void
	 */
	public function useBeta(bool $beta = false) {
		$this->beta = $beta;
	}
	
	/**
	 * Sets access credentials.
	 *
	 * @param String $username
	 *    Username.
	 * @param String $password
	 *    Password.
	 *
	 * @return void
	 */
	public function setCredentials(String $username, String $password) {
		$this->username = $username;
		$this->password = $password;
	}
	
	/**
	 * Performs a GET Request.
	 *
	 * @param String $resource
	 *    Resource.
	 * @param Array @query
	 *    Array of URL parameters.
	 *
     * @return \Guzzle\Http\Message\Response
     *   A Guzzle response.
     *
     * @throws \RuntimeException
     *   Thrown if the GET request failed.
	 */
	protected function get(string $resource, array $query = null, string $sink = null) {
		try {
			$headers = array(
				'auth' => [$this->username, $this->password],
				'headers' => ['Accept-Encoding' => 'gzip'],
				'allow_redirects' => [
					'strict' => true,
					'protocols' => ['https']
				]
			);
			
			if (isset($query)) $headers['query'] = $query;
			if (isset($sink)) $headers['sink'] = $sink;
			
			return $this->client->get($this->getBaseURL() . $resource, $headers);
		} catch (RequestException $e) {
			throw $e;
		}
	}
	
	/**
	 * Retrieves all schema.
	 *
	 * @return array
     *   Associative array of schemaKey and text.
	 */
	public function getSchema() {
		try {
			$response = $this->get("Schema");

			if ($response->getStatusCode() == 200) {
				$xml = new \SimpleXMLElement($response->getBody()->getContents());
				
				$result = [];
				
				foreach ($xml as $schema) {
					$attr = $schema->attributes();
					
					$schemaKey = Html::escape($attr['schemaKey']);
					$text = Html::escape($attr['text']);
					
					$result[$schemaKey] = $text;
				}
				
				return $result;
			}

		} catch (RequestException $e) {
			\Drupal::logger('feeds_dm')->error($e);
		}
		
		return false;
	}
	
	/**
	 * Retrieves all Intellectual Contribution Records.
	 *
	 * @param string $schemaKey
	 *   Schema Key.
	 * @param string $sink
	 *   Sink filename.
	 *
	 * @return Symfony\Component\HttpFoundation\Response
     *   Response from API call.
	 */
	public function getIntellCont(string $schemaKey, string $sink = null) {
		try {
			return $this->get("SchemaData/" . $schemaKey . "/INTELLCONT", null, $sink);
		} catch (RequestException $e) {
			\Drupal::logger('feeds_dm')->error($e);
		}
	}
	
	/**
	 * Helper function for decoding JSON Keys.
	 *
	 * @param $key_id
     *   Key Entity ID.
	 *
	 * @return array
     *  Decoded JSON Key.
	 */
	public static function decodeKey($key_id) {
		$key = \Drupal::service('key.repository')->getKey($key_id)->getKeyValue();
		
		return Json::decode($key);
	}
}