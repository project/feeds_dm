<?php

namespace Drupal\feeds_dm\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * The configuration form for the DM Fetcher.
 */
class DMFetcherForm extends ExternalPluginFormBase {

	/**
	 * {@inheritdoc}
	 */
	public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
		$form['api_key'] = [
			'#type' => 'key_select',
			'#title' => $this->t('API Key'),
			'#required' => true,
			'#key_filter' => [
				'type_group' => 'user_password'
			],
			'#default_value' => $this->plugin->getConfiguration('api_key')
		];
		
		$form['resource_url'] = [
			'#type' => 'select',
			'#title' => $this->t('Resource URL'),
			'#required' => true,
			'#options' => [
				'production' => $this->t('Production'),
				'beta' => $this->t('Beta')
			],
			'#default_value' => $this->plugin->getConfiguration('resource_url')
		];
		
		return $form;
	}

}
