<?php

namespace Drupal\feeds_dm\Feeds\Fetcher\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form on the feed edit page for the DMFetcher.
 */
class DMFetcherFeedForm extends ExternalPluginFormBase implements ContainerInjectionInterface {

	/**
	 * The Guzzle client.
	 *
	 * @var \GuzzleHttp\ClientInterface
	 */
	protected $client;

	/**
	 * Constructs an HttpFeedForm object.
	 *
	 * @param \GuzzleHttp\ClientInterface $client
	 *   The HTTP client.
	 */
	public function __construct(ClientInterface $client) {
		$this->client = $client;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container) {
		return new static($container->get('http_client'));
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildConfigurationForm(array $form, FormStateInterface $form_state, FeedInterface $feed = NULL) {  
		$configuration = $feed->getConfigurationFor($this->plugin);

		$service = \Drupal::service('dmws.client');

		$api_key = $service->decodeKey($this->plugin->getConfiguration('api_key'));
		$username = isset($api_key['username'])?$api_key['username']:'';
		$password = isset($api_key['password'])?$api_key['password']:'';

		$service->setCredentials($username, $password);

		if ($this->plugin->getConfiguration('resource_url') == 'beta') {
			$service->useBeta(true);
		}

		$form['schema'] = [
			'#type' => 'select',
			'#title' => $this->t('Schema'),
			'#required' => true,
			'#options' => $service->getSchema(),
			'#default_value' => $configuration['schema']
		];
		$form['resource'] = [
			'#type' => 'select',
			'#title' => $this->t('Resource'),
			'#required' => true,
			'#options' => [
				'intellcont' => $this->t('Intellectual Contributions'),
			],
			'#default_value' => $configuration['resource']
		];

		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function submitConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
		$feed->setConfigurationFor($this->plugin, $form_state->getValues());
	}
	
}
