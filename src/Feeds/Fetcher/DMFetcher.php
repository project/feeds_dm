<?php

namespace Drupal\feeds_dm\Feeds\Fetcher;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FeedInterface;
use Drupal\feeds\Plugin\Type\ClearableInterface;
use Drupal\feeds\Plugin\Type\Fetcher\FetcherInterface;
use Drupal\feeds\Plugin\Type\PluginBase;
use Drupal\feeds\Result\HttpFetcherResult;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Utility\Feed;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines an DM fetcher.
 *
 * @FeedsFetcher(
 *   id = "dm",
 *   title = @Translation("Digital Measures"),
 *   description = @Translation(""),
 *   form = {
 *     "configuration" = "Drupal\feeds_dm\Feeds\Fetcher\Form\DMFetcherForm",
 *     "feed" = "Drupal\feeds_dm\Feeds\Fetcher\Form\DMFetcherFeedForm",
 *   }
 * )
 */
class DMFetcher extends PluginBase implements ClearableInterface, FetcherInterface, ContainerFactoryPluginInterface {
	/**
	 * The Guzzle client.
	 *
	 * @var \GuzzleHttp\ClientInterface
	 */
	protected $client;

	/**
	 * Drupal file system helper.
	 *
	 * @var \Drupal\Core\File\FileSystemInterface
	 */
	protected $fileSystem;
  
	/**
	 * Constructs a DMFetcher object.
	 *
	 * @param array $configuration
	 *   The plugin configuration.
	 * @param string $plugin_id
	 *   The plugin id.
	 * @param array $plugin_definition
	 *   The plugin definition.
	 * @param \GuzzleHttp\ClientInterface $client
	 *   The Guzzle client.
	 * @param \Drupal\Core\File\FileSystemInterface $file_system
	 *   The Drupal file system helper.
	*/
	public function __construct(array $configuration, $plugin_id, array $plugin_definition, ClientInterface $client, FileSystemInterface $file_system) {
		$this->client = $client;
		$this->fileSystem = $file_system;

		parent::__construct($configuration, $plugin_id, $plugin_definition);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
		return new static(
			$configuration,
			$plugin_id,
			$plugin_definition,
			$container->get('http_client'),
			$container->get('file_system')
		);
	}

	/**
	 * {@inheritdoc}
	 */
	public function fetch(FeedInterface $feed, StateInterface $state) {
		$sink = $this->fileSystem->tempnam('temporary://', 'feeds_dm_fetcher');
		$sink = $this->fileSystem->realpath($sink);

		$service = \Drupal::service('dmws.client');

		$api_key = $service->decodeKey($this->configuration['api_key']);
		$username = isset($api_key['username'])?$api_key['username']:'';
		$password = isset($api_key['password'])?$api_key['password']:'';

		$service->setCredentials($username, $password);

		if ($this->configuration['resource_url'] == 'beta') {
			$service->useBeta(true);
		}

		$configuration = $feed->getConfigurationFor($this);
		$schema = $configuration['schema'];
		$resource = $configuration['resource'];

		switch ($resource) {
			case 'intellcont':
				$response = $service->getIntellCont($schema, $sink);
				break;
			default:
				break;
		}

		return new HttpFetcherResult($sink, $response->getHeaders());
	}

	/**
	 * {@inheritdoc}
	 */
	public function clear(FeedInterface $feed, StateInterface $state) {}

	/**
	 * {@inheritdoc}
	 */
	public function defaultConfiguration() {
		return [
			'api_key' => NULL,
			'resource_url' => NULL
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function defaultFeedConfiguration() {
		return [
			'schema' => NULL,
			'resource' => NULL
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function onFeedDeleteMultiple(array $feeds) {}

}
